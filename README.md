<div align="center">

# Profile on Panel

<img src="https://gitlab.gnome.org/euaaron/profile-on-panel/-/raw/master/profile-on-panel.png" alt="preview image" width="400"/>

A GNOME Shell Extension that add username@hostname and profile picture to the panel.
> Useful to show who you are while recording your screen for a tutorial.

</div>

## TODO

- Add config file where we can:
  - enable/disable username and/or hostname, or real name;
  - enable/disable profile pic;
  - choose text and image position;
  - make image round or square;

- add dropdown menu with links to any website where the user has a profile. Click on it must open the page with the default browser.
  
### Expected Default Options

- DeviantArt, GitHub, StackOverflow
- LinkedIn, Medium, DevTo, Reddit
- Facebook, Twitter,
- Custom

### Example of how the link should be

It's just a simple example that don't reflect on how it can really be made:

```html
<style>
  .online-profile {
    display: flex;
    justify-content: space-betwen;
    align-content: center;
  }
</style>

<a class="online-profile" href="link-to-the-profile-page">  
  <img src="link-to-the-profile-page/favicon"/>
  <strong>website username</strong>
</a>
```

## Author

[![Aaron Stiebler](https://secure.gravatar.com/avatar/2001db1f437bd2159b8eac932ebd49d5?s=128&d=identicon)](https://gitlab.gnome.org/euaaron)

## Contributors

No one yet... :/

## How to contribute

Just be yourself and contribute.

---
Ps.: this is my first extension.
