'use-strict'; // -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();

const { St, Gio, GLib, Clutter } = imports.gi;
const Main = imports.ui.main;

let panelImage, panelText, profilePic, username;

function init() {
    log(`Initializing ${Me.metadata.name} version ${Me.metadata.version}`);
    
    panelImage = new St.Bin({
        style_class: 'panelImage'
    });

    profilePic = new St.Icon({ 
        gicon: Gio.icon_new_for_string(
            '/var/lib/AccountsService/icons/' + 
            GLib.get_user_name()
        ), 
        style_class: 'profilePic',
    });

    panelText = new St.Bin({
        style_class: 'panelText'
    });

    username = new St.Label({
        style_class: 'username',
        text: GLib.get_user_name() + '@' + GLib.get_host_name()
    });

    profilePic.set_icon_size(22);

    panelText.set_child(username);
    panelImage.set_child(profilePic);

}

function enable() {
    log(`Enabling ${Me.metadata.name} version ${Me.metadata.version}`);
    Main.panel._rightBox.insert_child_at_index(panelText, -1);
    Main.panel._rightBox.insert_child_at_index(panelImage, -1);
}

function disable() {
    log(`Disabling ${Me.metadata.name} version ${Me.metadata.version}`);
    Main.panel._rightBox.remove_child(panelImage);
    Main.panel._rightBox.remove_child(panelText);
}
